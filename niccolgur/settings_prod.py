from niccolgur.settings import *
import os

DEBUG = os.getenv("NICCOLGUR_DEBUG", 'False').lower() in ('true', '1', 't')

SECRET_KEY = os.environ["SECRET_KEY"]

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

# See https://pypi.org/project/django-cors-headers/
CORS_ALLOW_ALL_ORIGINS = False
CORS_ALLOWED_ORIGINS = [
    'http://api.guglielmofelici.com',
    'https://niccolgur.guglielmofelici.com',
    'https://niccolgur2.guglielmofelici.com',
    'https://niccoldur.guglielmofelici.com',
    'http://localhost',
]