"""niccolgur URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework.routers import DefaultRouter
from django.urls import path, include, register_converter
from django.contrib import admin
from django.urls import path
from main import views
from rest_framework.authtoken import views as drf_views

router = DefaultRouter()
router.register(r"users", views.CustomUserViewSet, basename="users"),
router.register(r"niccolgurs", views.NiccolgurViewSet, basename="niccolgurs")
router.register(r"niccuscars", views.NiccuscarViewSet, basename="niccuscars")


urlpatterns = [
    path("", include(router.urls)),
    path("build/", views.build_date),
    path("hash-password/<str:pw>", views.hash_password),
    path("queue/", views.queue),
    path("queue/shift/", views.shift_queue),
    path("queue/shift_user/<str:id>/", views.shift_user),
    path("search_tmdb_movie/", views.search_tmdb_movie),
    path("seasons/count/", views.season_count),
    path("seasons/<str:id>/", views.season),
    # path('seasons/<str:id>', views.season_last),
    path("api-auth/", include("rest_framework.urls")),
    path("api-token-auth/", views.CustomAuthToken.as_view()),
]
