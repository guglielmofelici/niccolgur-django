#!/bin/bash

source scripts/.verify-env.bash

echo "WARNING: Existing records will not be deleted. If you want a clean db init, stop the app, delete the data directory, restart the app and run this script again."

docker exec ${DB_CONTAINER_NAME}-${env} sh -c "psql -U postgres -d postgres -f /var/lib/postgresql/data/init.sql" && \
echo "To be able to log in, run docker exec -it ${BE_CONTAINER_NAME}-${env} sh -c 'python manage.py changepassword guglielmo'"
