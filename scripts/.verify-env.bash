# source this script to have an automatic handling of environments in other scripts

if [ $# -lt 1 ]; then
    echo "Specify an environment: dev, prod"
    exit 1
fi

env=$1

if [[ ! $env =~ dev|prod ]]; then
    echo "Specify an environment from: dev, prod"
    exit 1
fi

source config/env.${env}
