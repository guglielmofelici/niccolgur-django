#!/bin/bash

docker build -t guglielmofelici/niccolgur-be . && docker push guglielmofelici/niccolgur-be || {
   echo "Push failed, trying to login..." && docker login && docker push guglielmofelici/niccolgur-be;
}


# ARM
#docker run --privileged --rm tonistiigi/binfmt --install all
#docker buildx create --use
#docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --tag guglielmofelici/niccolgur-be --push . || { echo "May need to run as sudo." && exit 1; }

# OLD
#docker tag niccolgur-be guglielmofelici/niccolgur-be
#docker push guglielmofelici/niccolgur-be || {
#    echo "Push failed, try to login..." && docker login && docker push guglielmofelici/niccolgur-be;
#}
