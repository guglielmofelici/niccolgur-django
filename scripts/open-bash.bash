#!/bin/bash

source scripts/.verify-env.bash

sudo docker exec -it ${BE_CONTAINER_NAME}-$1 /bin/bash
