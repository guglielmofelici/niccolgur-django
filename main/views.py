from multiprocessing.sharedctypes import Value
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework import viewsets
from .models import CustomUser, Niccolgur, MovieData, Niccuscar
import requests
from .serializers import (
    CustomUserReadSerializer,
    CustomUserWriteSerializer,
    NiccolgurSerializer,
    NiccuscarSerializer
)
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db.models import Max, Count
from django.http import HttpResponseBadRequest, HttpResponseNotFound, HttpResponseServerError
from django.db import transaction
import os
from rest_framework.decorators import action
from datetime import datetime
from rest_framework.permissions import IsAuthenticatedOrReadOnly

class CustomUserViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CustomUserReadSerializer
        return CustomUserWriteSerializer
    

    @action(detail=True, methods=["get"])
    def stats(self, request, pk=None, *args, **kwargs):
        user = CustomUser.objects.filter(pk=pk).first()
        hits = Niccolgur.objects.filter(members=user).count()
        misses = Niccolgur.objects.exclude(members=user).count()
        ranks = self._rankings()
        ranks = list(ranks.values_list('username', flat=True))
        rank = 0
        if user.username in ranks:
            rank = ranks.index(user.username) + 1
        return Response({"username": user.username, "id": user.id, "hits": hits, "misses": misses, "rank": rank})
    
    @action(detail=False, methods=["get"])
    def rankings(self, request, *args, **kwargs):
        rankings = self._rankings()
        serialized = CustomUserReadSerializer(rankings, many=True)
        return Response(serialized.data)
    
    
    def _rankings(self):
        return (
            CustomUser.objects
            .annotate(hits=Count('participated_in'))
            .order_by('-hits')
        )

class NiccolgurViewSet(viewsets.ModelViewSet):
    queryset = Niccolgur.objects.all()
    serializer_class = NiccolgurSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def list(self, request, *args, **kwargs):
        userid = request.GET.get("user")
        if userid:
            if not CustomUser.objects.get(pk=userid):
                return HttpResponseNotFound()
            queryset = Niccolgur.objects.filter(master=userid).order_by('-date')
        else:
            # TODO pagination
            return HttpResponseBadRequest("You must specify a filter")
        serializer = NiccolgurSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = request.data
        if not request.user.is_staff or "master" not in data:
            data["master"] = request.user.pk
        if not request.user.is_staff or "season" not in data:
            data["season"] = _season_count()
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        first_in_queue = CustomUser.objects.get(queue=1)
        if first_in_queue.id != validated_data["master"].id:
            return Response(status=400, 
                data={"details": "The master is not the first in queue at the moment."}
            )
        if (validated_data["master"] not in validated_data["members"]):
            return Response(status=400, 
                data={"details": "The master must be present to its own niccolgur."}
            )
        if validated_data["date"].weekday() != 4:  # 4 represents Friday
            return Response(status=400, 
                data={"details": "The supplied date is not a Friday."}
            )
        with transaction.atomic():
            try:
                movie = self.create_movie_data_from_tmdb(validated_data["movie_id"])
                serializer.validated_data["movie_data_id"] = movie.id
            except ValueError as e:
                return Response({"details": f"Could not find the movie {validated_data['movie_id']}: {e}"}, status=400)
            except RuntimeError as e:
                return Response({"details": f"Error creating the movie {validated_data['movie_id']}: {e}"}, status=400)
            self.perform_create(serializer)
            _shift_queue(serializer.data["members"])
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=201, headers=headers)
    
    def create_movie_data_from_tmdb(self, movie_id):
        tmdb_api_key = os.environ['TMDB_API_KEY']
        tmdb_data = requests.get(f'https://api.themoviedb.org/3/movie/{movie_id}?api_key={tmdb_api_key}&language=it')
        if tmdb_data.status_code == 404:
            raise ValueError(tmdb_data.text)
        elif tmdb_data.status_code != 200:
            raise RuntimeError(tmdb_data.text)
        tmdb_data = tmdb_data.json()
        return MovieData.objects.create(
            tmdb_id = movie_id,
            title = tmdb_data.get("title"),
            original_title = tmdb_data.get("original_title"),
            tagline = tmdb_data.get("tagline"),
            overview = tmdb_data.get("overview", "")[0:1024],
            poster_path = tmdb_data.get("poster_path"),
            budget = tmdb_data.get("budget"),
            country = tmdb_data.get("origin_country", [None])[0],
            language = tmdb_data.get("original_language"),
            release_date = tmdb_data.get("release_date"),
            raw_json = tmdb_data,
        )

class NiccuscarViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Niccuscar.objects.all()
    serializer_class = NiccuscarSerializer

    def list(self, request, *args, **kwargs):
        queryset = Niccuscar.objects.all()
        serializer = NiccuscarSerializer(queryset, many=True)
        return Response(serializer.data)

@api_view(["GET"])
def search_tmdb_movie(request, *args, **kwargs):
    query = request.GET.get('query')
    resp = requests.get(f'https://api.themoviedb.org/3/search/movie?query={query}&api_key={os.environ["TMDB_API_KEY"]}&language=it')
    try:
        return Response(status=resp.status_code, data=resp.json())
    except:
        return Response(status=resp.status_code, data=resp.text)

@api_view(["GET"])
def season(request, id):
    try:
        id = int(id)
    except ValueError:
        return HttpResponseBadRequest("The id must be convertible to an integer")
    if id < 0:
        id = _season_count()
    queryset = Niccolgur.objects.filter(season=id).order_by("-date")
    serializer = NiccolgurSerializer(queryset, many=True)
    return Response(serializer.data)


@api_view(["GET"])
def season_count(_=None):
    return Response(_season_count())



def _season_count():
    return Niccolgur.objects.aggregate(Max("season"))["season__max"]


def _queue():
    queryset = CustomUser.objects.order_by("queue")
    serializer = CustomUserReadSerializer(queryset, many=True)
    return Response(serializer.data)


def _shift_queue(members):
    users = CustomUser.objects.all().order_by("-queue")
    total_users = users.count()
    master = users.last()  # It's ordered by queue desc
    absents = [user for user in users if user.id not in members]
    with transaction.atomic():
        for user in absents:
            _shift_user_times(user, 2, lock_absents=True, absents=absents)
        _shift_user_times(master, total_users)


def _shift_user(user: CustomUser, lock_absents=False, absents=[]):
    if user is None:
        raise ValueError
    user.refresh_from_db()
    user_current_pos = user.queue
    with transaction.atomic():
        new_pos = user.queue + 1
        next_user = CustomUser.objects.filter(queue=new_pos).exclude(id=user.id).first()
        if next_user and (not lock_absents or next_user not in absents):
            next_user.queue = user_current_pos
            next_user.save()
            user.queue = new_pos
            user.save()


def _shift_user_times(user, n, **kwargs):
    for _ in range(n):
        _shift_user(user, **kwargs)


@api_view(["GET"])
def queue(request):
    return _queue()


@api_view(["POST"])
def shift_user(request, id, *args, **kwargs):
    try:
        user = CustomUser.objects.get(pk=id)
    except CustomUser.DoesNotExist:
        return HttpResponseNotFound()
    _shift_user(user)
    return _queue()


@api_view(["POST"])
def shift_queue(request, *args, **kwargs):
    if CustomUser.objects.get(queue=1) not in request.data:
        return HttpResponseBadRequest(
            "The current master must be present in order to shift the queue."
        )
    try:
        _shift_queue(request.data)
    except ValueError:
        return HttpResponseBadRequest()
    return _queue()


class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)
        return Response(
            {"token": token.key, "user": CustomUserReadSerializer(instance=user).data}
        )


@api_view(["GET"])
def hash_password(request, pw):
    return Response(make_password(pw))


@api_view(["GET"])
def build_date(request):
    try:
        with open("build_date.txt", "r") as f:
            return Response(f.readline().strip())
    except FileNotFoundError:
        return Response("Build date not available", status=500)
