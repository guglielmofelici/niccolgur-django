from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.deletion import DO_NOTHING, SET_NULL, CASCADE


class CustomUser(AbstractUser):

    queue = models.PositiveIntegerField()
    nickname = models.CharField(max_length=50)
    bio = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.username


class MovieData(models.Model):
    tmdb_id = models.IntegerField(unique=True)
    title = models.CharField(max_length=255)
    original_title = models.CharField(max_length=255, null=True)
    tagline = models.CharField(max_length=255, null=True)
    poster_path = models.CharField(max_length=255, null=True)
    overview = models.CharField(max_length=1024, null=True)
    budget = models.BigIntegerField(null=True)
    country = models.CharField(max_length=8, null=True)
    language = models.CharField(max_length=8, null=True)
    release_date = models.DateField(null=True)
    raw_json = models.JSONField(null=True)
    

class Niccolgur(models.Model):

    master = models.ForeignKey(
        CustomUser, related_name="niccolgurs", on_delete=DO_NOTHING
    )
    movie_id = models.CharField(max_length=20)
    members = models.ManyToManyField(CustomUser, "participated_in")
    date = models.DateField()
    season = models.PositiveIntegerField()
    movie_data = models.ForeignKey(
        MovieData,
        related_name="niccolgur",
        on_delete=SET_NULL,
        null=True,
    )

class Niccuscar(models.Model):
    niccolgur = models.ForeignKey(
        Niccolgur, related_name='niccuscars', on_delete=CASCADE
    )
    year = models.IntegerField()
    category = models.CharField(choices=[
        ('BEST', 'Best movie'),
        ('WORST', 'Worst movie'),
        ('BEST_FROM_AUTHOR', 'Best movie from this Master'),
        ('WORST_FROM_AUTHOR', 'Worst movie from this Master'),
        ('HARD_TO_WATCH', 'Hardest movie to watch'),
        ('MEMES', 'Best memes'),
        ('TITS', 'Best tetteculofica'),
        ('UNEXPECTED', 'Most unexpected'),
        ('THROW', 'Best throw')
        ])