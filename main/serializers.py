from rest_framework import serializers
from .models import *


class CustomUserReadSerializer(serializers.ModelSerializer):
    hits = serializers.SerializerMethodField()

    class Meta:
        model = CustomUser
        fields = ["id", "username", "nickname", "bio", "queue", "hits", "is_active", "is_staff"]

    def get_hits(self, obj):
        return getattr(obj, "hits", None)

class CustomUserWriteSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = '__all__'


class NiccuscarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Niccuscar
        fields = '__all__'


class MovieDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = MovieData
        exclude = ('raw_json',)
        
class NiccolgurSerializer(serializers.ModelSerializer):
    movie_data = MovieDataSerializer(read_only=True)
    niccuscars = NiccuscarSerializer(many=True)

    class Meta:
        model = Niccolgur
        fields = '__all__'
