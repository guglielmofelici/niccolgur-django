from unittest import mock
from rest_framework.test import APITestCase
from rest_framework import status
from main.models import CustomUser
from main import views
from unittest.mock import ANY


class QueueTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        for idx, user in enumerate(
            [
                "diarca",
                "castellano",
                "paesano",
                "contadino",
                "mercante",
            ]
        ):
            CustomUser.objects.create(
                username=user, nickname=user, queue=idx + 1, bio=""
            )

    def test__shift_user(self):
        self._expect_queue(
            [
                "diarca",
                "castellano",
                "paesano",
                "contadino",
                "mercante",
            ]
        )
        views._shift_user(self._get_user("diarca"))
        self._expect_queue(
            [
                "castellano",
                "diarca",
                "paesano",
                "contadino",
                "mercante",
            ]
        )
        views._shift_user(self._get_user("castellano"))
        self._expect_queue(
            [
                "diarca",
                "castellano",
                "paesano",
                "contadino",
                "mercante",
            ]
        )
        views._shift_user(self._get_user("mercante"))
        self._expect_queue(
            [
                "diarca",
                "castellano",
                "paesano",
                "contadino",
                "mercante",
            ]
        )
        views._shift_user(
            self._get_user("paesano"),
            lock_absents=True,
            absents=[self._get_user("contadino")],
        )
        self._expect_queue(
            [
                "diarca",
                "castellano",
                "paesano",
                "contadino",
                "mercante",
            ]
        )

    def test__shift_user_times(self):
        views._shift_user_times(self._get_user("diarca"), 1)
        self._expect_queue(
            [
                "castellano",
                "diarca",
                "paesano",
                "contadino",
                "mercante",
            ]
        )
        views._shift_user_times(self._get_user("castellano"), 2)
        self._expect_queue(
            [
                "diarca",
                "paesano",
                "castellano",
                "contadino",
                "mercante",
            ]
        )
        views._shift_user_times(self._get_user("paesano"), 20)
        self._expect_queue(
            [
                "diarca",
                "castellano",
                "contadino",
                "mercante",
                "paesano",
            ]
        )

    def test__shift_queue(self):
        self._expect_queue(
            [
                "diarca",
                "castellano",
                "paesano",
                "contadino",
                "mercante",
            ]
        )
        views._shift_queue([self._get_user("diarca").id, self._get_user("paesano").id])
        self._expect_queue(
            [
                "paesano",
                "castellano",
                "contadino",
                "mercante",
                "diarca",
            ]
        )
        views._shift_queue(
            [self._get_user("paesano").id, self._get_user("castellano").id]
        )
        self._expect_queue(
            [
                "castellano",
                "contadino",
                "mercante",
                "diarca",
                "paesano",
            ]
        )
        views._shift_queue([self._get_user("castellano").id])
        self._expect_queue(
            [
                "contadino",
                "mercante",
                "diarca",
                "paesano",
                "castellano",
            ]
        )
        views._shift_queue(
            [self._get_user("contadino").id, self._get_user("castellano").id]
        )
        self._expect_queue(
            [
                "castellano",
                "mercante",
                "diarca",
                "paesano",
                "contadino",
            ]
        )
        views._shift_queue(
            [
                self._get_user("castellano"),
                self._get_user("paesano").id,
                self._get_user("contadino").id,
            ]
        )
        self._expect_queue(
            [
                "paesano",
                "contadino",
                "mercante",
                "diarca",
                "castellano",
            ]
        )

    def _get_user(self, username):
        return CustomUser.objects.get(username=username)

    def _expect_queue(self, queue):
        for position, user in enumerate(queue):
            self.assertEqual(
                self._get_user(user).queue,
                position + 1,
                f'User {user} in wrong place. Queue: {CustomUser.objects.order_by("queue").values("username", "queue")}',
            )
