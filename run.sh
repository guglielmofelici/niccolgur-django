#!/bin/bash

function update_and_run {
    case $1 in
    dev)
        docker compose -f docker-compose.development.yml --env-file "config/env.dev" up --build
        outcode=$?
        if [ $outcode -eq 17 ]; then echo "Maybe run as sudo?";
        elif [ $outcode -eq 130 ]; then 
            read -p "Do you want to run docker image prune to reclaim disk space? [y/N]" prune
            if [[ $prune =~ [yY] ]]; then
                docker image prune; # ask to prune images to avoid cluttering disk
            fi
        fi
    ;;
    prod)
        docker compose --env-file config/env.prod pull && docker compose -f docker-compose.yml --env-file config/env.prod up
        if [ $? -eq 17 ]; then echo "Maybe run as sudo?"; fi
    ;;
    -h|help|--help)
        echo "Run with ./run.sh [dev|prod] to start the service in development or production environment."
        exit 0
    ;;
    *)
        echo "Available environments are: dev, prod"
    ;;
    esac
}

if [ $# -lt 1 ]; then
    read -p "Specify one from dev, prod : " env
else
    env=$1
fi
update_and_run $env
