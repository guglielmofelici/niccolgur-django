FROM python:3.10

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /code
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .

# So that we can know when the image was built
RUN date > /code/build_date.txt

EXPOSE 8000
CMD ["python manage.py runserver 0.0.0.0:8000"]
